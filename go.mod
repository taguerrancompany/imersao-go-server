module gitlab.com/taguerrancompany/imersao-go-server

go 1.20

require github.com/google/uuid v1.3.0

require github.com/confluentinc/confluent-kafka-go/v2 v2.1.1

package main

import (
	"encoding/json"
	ckafka "github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"gitlab.com/taguerrancompany/imersao-go-server/internal/ifra/kafka"
	"gitlab.com/taguerrancompany/imersao-go-server/internal/market/DTO"
	"gitlab.com/taguerrancompany/imersao-go-server/internal/market/investor/entity"
	"gitlab.com/taguerrancompany/imersao-go-server/internal/market/transformer"
	"sync"
)

func main() {
	orderIN := make(chan *entity.Order)
	orderOUT := make(chan *entity.Order)
	wg := &sync.WaitGroup{}
	defer wg.Wait()

	kafkaMsgChan := make(chan *ckafka.Message)
	configMap := &ckafka.ConfigMap{
		"bootstrap.servers": "host.docker.internal:9094",
		"group.id":          "mygroup",
		"auto.offset.reset": "earliest"}

	producer := kafka.NewProducer(configMap)
	kafka := kafka.NewConsumer(configMap, []string{"input"})

	go kafka.Consume(kafkaMsgChan) //T2
	//recebe do ch kafka joga no input processa no output e joga no kafka
	book := entity.NewBook(orderIN, orderOUT, wg)
	go book.Trade() //T3

	go func() {
		for msg := range kafkaMsgChan {
			wg.Add(1)
			tradeInput := DTO.TradeIn{}
			err := json.Unmarshal(msg.Value, &tradeInput)
			if err != nil {
				panic(err)
			}
			order := transformer.TransformImput(tradeInput)
			orderIN <- order
		}
	}()

	for res := range orderOUT {
		output := transformer.TransformOutput(res)
		outputJson, err := json.MarshalIndent(output, "", "  ")
		if err != nil {
			println(err)
		}
		producer.Publish(outputJson, []byte("orders"), "output")
	}
}

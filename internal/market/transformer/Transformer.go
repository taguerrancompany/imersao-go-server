package transformer

import (
	"gitlab.com/taguerrancompany/imersao-go-server/internal/market/DTO"
	"gitlab.com/taguerrancompany/imersao-go-server/internal/market/investor/entity"
)

func TransformImput(input DTO.TradeIn) *entity.Order {
	asset := entity.NewAsset(input.AssetID, input.AssetID, 1000)
	investor := entity.NewInvestor(input.InvestorID)
	order := entity.NewOrder(input.OrderID, investor, asset, input.Shares, input.Price, input.OrderType)

	if input.CurrentShares > 0 {
		assetPosition := entity.NewInvestorAsssetPosition(input.AssetID, input.CurrentShares)
		investor.AddAssetPosition(assetPosition)
	}
	return order
}

func TransformOutput(order *entity.Order) *DTO.OrderOutput {
	output := &DTO.OrderOutput{
		OrderID:    order.ID,
		InvestorID: order.Investor.Id,
		AssetID:    order.Asset.ID,
		OrderType:  order.OrderType,
		Status:     order.Status,
		Shares:     order.Shares,
		Partial:    order.PendingShares,
	}
	var transactionsOutput []*DTO.TransactionOutput

	for _, t := range order.Transactions {
		transactionOutput := &DTO.TransactionOutput{
			TransactionID: t.ID,
			BuyerID:       t.BuyingOrder.Investor.Id,
			SellerID:      t.SellingOrde.Investor.Id,
			AssetID:       t.SellingOrde.Asset.ID,
			Price:         t.Price,
			Shares:        t.SellingOrde.Shares - t.SellingOrde.PendingShares,
		}
		transactionsOutput = append(transactionsOutput, transactionOutput)
	}
	output.TransactionsOutput = transactionsOutput

	return output
}

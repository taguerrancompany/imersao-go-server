package entity

type OrderQueue struct {
	Orders []*Order
}

// essa função verifica se i é maior que jota e retorna bool
func (oq *OrderQueue) Less(i, j int) bool {
	return oq.Orders[i].Price < oq.Orders[j].Price
}

// essa fun deve inverter as posições i vira j e j vira i
func (oq *OrderQueue) Swap(i, j int) {
	oq.Orders[i], oq.Orders[j] = oq.Orders[j], oq.Orders[i]
}

// essa fuc deve retornar um inteiro com o tamanho da order
func (qo *OrderQueue) Len() int {
	return len(qo.Orders)
}

// essa fun deve  adicionar uma order
func (oq *OrderQueue) Push(x interface{}) {
	oq.Orders = append(oq.Orders, x.(*Order))
}

// essa func deve remover o ultimo item
func (oq *OrderQueue) Pop() interface{} {
	old := oq.Orders
	n := len(old)
	item := old[n-1]
	oq.Orders = old[0 : n-1]
	return item

}

// cria uma nova order
func NewOrderQueue() *OrderQueue {
	return &OrderQueue{}
}

package entity

type Investor struct {
	Id            string
	Name          string
	AssetPosition []*InvestorAssetPosition
}

func (i *Investor) AddAssetPosition(assetPosition *InvestorAssetPosition) {
	i.AssetPosition = append(i.AssetPosition, assetPosition)
}

func (i *Investor) UpdatePosition(assetID string, qtdShares int) {
	assetPosition := i.GetAssetPosition(assetID)
	if assetPosition == nil {
		i.AssetPosition = append(i.AssetPosition, NewInvestorAsssetPosition(assetID, qtdShares))
	} else {
		assetPosition.Shares += qtdShares
	}
}

func NewInvestorAsssetPosition(assetID string, shares int) *InvestorAssetPosition {

	return &InvestorAssetPosition{
		AssetID: assetID,
		Shares:  shares,
	}
}

func (i *Investor) GetAssetPosition(AssetID string) *InvestorAssetPosition {

	for _, assetPOsition := range i.AssetPosition {
		if assetPOsition.AssetID == AssetID {
			return assetPOsition
		}
	}
	return nil
}
func NewInvestor(id string) *Investor {
	return &Investor{
		Id: id,
	}
}

type InvestorAssetPosition struct {
	AssetID string
	Shares  int
}
